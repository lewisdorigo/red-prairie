<?php
/**
 * Plugin Name: WooCommerce Red Prairie Integration
 * Plugin URI: https://dorigo.co/
 * Description: Allows WooCommerce to work with Red Prairie’s Fulfillment thing
 * Author: Lewis Dorigo
 * Author URI: https://dorigo.co/
 * Version: 1.0.9
 */

namespace Dorigo;

if(!defined('ABSPATH')) { exit; }



class WC_RedPrairie_Integration {
    const VERSION = '1.0.0';
    protected static $instance;

    private function __construct() {

        if(class_exists('WC_Integration') &&
           defined('WOOCOMMERCE_VERSION') &&
           version_compare(WOOCOMMERCE_VERSION, '3.0.0', '>=')) {

            require_once 'lib/wc-redprairie.class.php';

            add_filter('woocommerce_integrations', [$this, 'add_integration']);
        } else {
            add_action('admin_notices', [$this, 'wc_missing']);
        }

        add_filter('plugin_action_links_'.plugin_basename(__FILE__), [$this, 'plugin_links']);
        add_action('admin_enqueue_scripts', [$this, 'admin_js']);

        add_action('wp_ajax_woocommerce_red_prairie_do_import', [$this,'removeEvent'], 0);
        add_action('wp_ajax_woocommerce_red_prairie_do_import', [$this,'addEvent'], 1);
    }

    public static function get_instance() {
        if(self::$instance === null) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function add_integration($integrations) {
        $integrations[] = 'Dorigo\\WC_RedPrairie';
        return $integrations;
    }

    public function plugin_links($links) {
        $settings = add_query_arg([
            'page' => 'wc-settings',
            'tab'  => 'integration',
            'section' => 'red_prairie'
        ], admin_url('admin.php'));

        $plugin_links = [
            '<a href="'.esc_url($settings).'">Settings</a>',
        ];

        return array_merge($plugin_links, $links);
    }

    public function wc_missing() {
        echo '<div class="error"><p>WooCommerce Red Prairie depends on the last version of <a href="http://www.woothemes.com/woocommerce/" target="_blank">WooCommerce</a> to work!</p></div>';
    }

    public static function addEvent() {
        wp_schedule_event(time(), 'hourly', 'woocommerce_red_prairie_import');
    }

    public static function removeEvent() {
        wp_clear_scheduled_hook('woocommerce_red_prairie_import');
    }

    public function admin_js() {
        wp_enqueue_script('red_prairie_js', plugin_dir_url(__FILE__) . '/assets/js.js', ['jquery'], null, true);
    }
}

add_action('plugins_loaded', [__NAMESPACE__.'\\WC_RedPrairie_Integration', 'get_instance'], 0);

register_activation_hook(__FILE__,   [__NAMESPACE__.'\\WC_RedPrairie_Integration', 'addEvent']);
register_deactivation_hook(__FILE__, [__NAMESPACE__.'\\WC_RedPrairie_Integration', 'removeEvent']);
