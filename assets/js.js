(function($) {
  $('#woocommerce_red_prairie_rp_ftp_update').on('click', function(e) {
    var $this = $(this),
        $parent = $this.closest('fieldset');

    console.log('clicked');

    $.ajax({
      'url'      : ajaxurl,
      'data'     : {
        'action' : 'woocommerce_red_prairie_do_import',
      },
      'method'   : 'post',
      'cache'    : false,
      'datatype' : 'json',
      'complete' : (function(e, status) {
        if(status === 'success') {
          $parent.find('.description strong').html('just now');
        } else {
          $parent.find('.description').html('An unknown error occurred. Please try again later.');
        }
      }),
    })
  });
})(jQuery);
