<?php

namespace Dorigo;

class WC_RedPrairie extends \WC_Integration {
    private $ftp = null;

    public function __construct() {
        $this->id                 = 'red_prairie';
        $this->method_title       = 'Red Prairie';
        $this->method_description = 'Red Prairie is warehouse management system, used by many fulfilment companies.';
        $this->last_update        = get_option('woocommerce_rp_last_update', 0);

        $this->checkFields();

        $this->init_form_fields();
        $this->init_settings();

		$constructor = $this->init_options();

        add_action('woocommerce_payment_complete', [$this, 'orderPaid']);
        //add_action('woocommerce_process_shop_order_meta', [$this, 'orderUpdated']);
        add_action('save_post', [$this, 'orderSaved'], 11, 1);


        //add_action('woocommerce_order_status_on_hold',   [$this, 'orderDelete']);
        //add_action('woocommerce_order_status_cancelled', [$this, 'orderDelete']);
        //add_action('woocommerce_order_status_refunded',  [$this, 'orderDelete']);

        add_action('woocommerce_update_options_integration_'.$this->id, [$this, 'process_admin_options']);

        add_action('woocommerce_red_prairie_import', [$this, 'importFiles']);

        add_action('wp_ajax_woocommerce_red_prairie_do_import', [$this,'sendOrders'], 9, 0);
        add_action('wp_ajax_woocommerce_red_prairie_do_import', [$this,'doAjax']);

        add_filter('woocommerce_shipping_instance_form_fields_flat_rate', [$this, 'addDeliveryDateFields']);
    }


    public function addDeliveryDateFields($fields) {

        $fields['shipping_days'] = [
            'title'       => 'Shipping Days (Minimum)',
            'type'        => 'number',
            'description' => 'The minimum number of days this shipping method will generally take.',
            'default'     => '3',
            'desc_tip'    => true
        ];

        $fields['shipping_to_days'] = [
            'title'       => 'Shipping Days (Maximum)',
            'type'        => 'number',
            'description' => 'The maximum number of days this shipping method will generally take.',
            'default'     => '',
            'desc_tip'    => true
        ];

        $fields['weekend_delivery'] = [
            'title'       => 'Weekend Delivery',
            'type'        => 'checkbox',
            'label'       => 'Will this shipping method deliver on weekends?',
            'default'     => 'no',
        ];

        $fields['cutoff_time'] = [
            'title'       => 'Cutoff Time',
            'type'        => 'select',
            'description' => 'At what time should the date for shipping switch over?',
            'default'     => 'no',
            'class'       => 'wc-enhanced-select',
            'options'     => [
                '1200'     => '12:00pm',
                '1300'     => '1:00pm',
                '1400'     => '2:00pm',
                '1500'     => '3:00pm',
                '1600'     => '4:00pm',
                '1700'     => '5:00pm',
                '1800'     => '6:00pm',
                '1900'     => '7:00pm',
                '2000'     => '8:00pm',
                '2100'     => '9:00pm',
                '2200'     => '10:00pm',
                '2300'     => '11:00pm',
            ],

            'desc_tip'    => true
        ];

        return $fields;
    }

    public function doAjax() {
        $return = [
            'status' => 'success',
            'timestamp' => time()
        ];

        try {
            $this->importFiles();
        } catch(Exception $e) {
            $return['status'] = 'error';
            $return['message'] = $e->getMessage();
        }

        echo json_encode($return);
        die();
    }


    public function checkFields() {
        $this->ftp_host = defined('RP_HOST') ? RP_HOST : null;
        $this->ftp_user = defined('RP_USER') ? RP_USER : null;
        $this->ftp_pass = defined('RP_PASS') ? RP_PASS : null;
        $this->ftp_path = defined('RP_PATH') ? RP_PATH : '/';

        $this->rp_client = defined('RP_CLIENT') ? RP_CLIENT : null;
        $this->rp_ware   = defined('RP_WARE')   ? RP_WARE   : null;
        $this->rp_prefix = defined('RP_PREFIX') ? RP_PREFIX : '';

        if(!$this->ftp_host || !$this->ftp_user || !$this->rp_client || !$this->rp_ware) {
            add_action('admin_notices', [$this, 'settings_notice']);
        }
    }


	public function init_options() {
		$options = array(
			'rp_development',
			'rp_last_update',
		);

		$constructor = array();
		foreach ($options as $option) {
			$constructor[$option] = $this->$option = $this->get_option($option);
		}

		return $constructor;
	}


    public function settings_notice() {
        $settings = add_query_arg([
            'page' => 'wc-settings',
            'tab'  => 'integration',
            'section' => 'red_prairie'
        ], admin_url('admin.php'));

        echo '<div class="error"><p><a href="'.$settings.'">WooCommerce Red Prairie</a> requires some settings to be defined.</p></div>';
    }

    public function generate_button_html($key, $data) {
        $field = "{$this->plugin_id}{$this->id}_{$key}";
        $defaults = [
            'class'             => 'button-secondary',
            'css'               => '',
            'custom_attributes' => [],
            'desc_tip'          => false,
            'description'       => '',
            'title'             => '',
        ];

        $data = wp_parse_args($data, $defaults);

        ob_start() ?>

            <tr valign="top">
                <th scope="row" class="titledesc">
                    <label for="<?= esc_attr($field); ?>"><?= wp_kses_post($data['title']); ?></label>
                    <?= $this->get_tooltip_html($data); ?>
                </th>
                <td class="forminp">
                    <fieldset>
                        <label class="screen-reader-text">
                            <span><?= wp_kses_post($data['title']); ?></span>
                        </label>
                        <button class="<?= esc_attr($data['class']); ?>" type="button" name="<?= esc_attr($field); ?>" id="<?= esc_attr($field); ?>" style="<?= esc_attr($data['css']); ?>" <?= $this->get_custom_attribute_html($data); ?>><?= wp_kses_post($data['title']); ?></button>
        				<?= $this->get_description_html($data); ?>
                    </fieldset>
                </td>
            </tr>

        <?php
        return ob_get_clean();
    }


    public function init_form_fields() {

        $this->form_fields = [
            'rp_ftp_host' => [
                'title'       => 'FTP Host',
                'description' => 'This should be defined as <code>RP_HOST</code> in your <code>wp_config.php</code>',
                'type'        => 'url',
                'default'     => '',
                'placeholder' => $this->ftp_host,
                'disabled'    => true,
            ],
            'rp_ftp_username' => [
                'title'       => 'FTP Username',
                'description' => 'This should be defined as <code>RP_USER</code> in your <code>wp_config.php</code>',
                'type'        => 'text',
                'placeholder' => $this->ftp_user,
                'default'     => '',
                'disabled'    => true,
            ],
            'rp_ftp_password' => [
                'title'       => 'FTP Password',
                'description' => 'This should be defined as <code>RP_PASS</code> in your <code>wp_config.php</code>',
                'type'        => 'password',
                'placeholder' => $this->ftp_pass ? str_repeat('•',strlen($this->ftp_pass)) : null,
                'default'     => '',
                'disabled'    => true,
            ],
            'rp_ftp_path' => [
                'title'       => 'FTP Path',
                'description' => 'This should be defined as <code>RP_PATH</code> in your <code>wp_config.php</code>',
                'type'        => 'text',
                'placeholder' => $this->ftp_path,
                'default'     => '',
                'disabled'    => true,
            ],
            'rp_ftp_client' => [
                'title'       => 'Client ID',
                'description' => 'This should be defined as <code>RP_CLIENT</code> in your <code>wp_config.php</code>',
                'type'        => 'text',
                'placeholder' => $this->rp_client,
                'default'     => '',
                'disabled'    => true,
            ],
            'rp_ftp_warehouse' => [
                'title'       => 'Warehouse ID',
                'description' => 'This should be defined as <code>RP_WARE</code> in your <code>wp_config.php</code>',
                'type'        => 'text',
                'placeholder' => $this->rp_ware,
                'default'     => '',
                'disabled'    => true,
            ],
            'rp_prefix' => [
                'title'       => 'Order Number Prefix',
                'description' => 'This should be defined as <code>RP_PREFIX</code> in your <code>wp_config.php</code>',
                'type'        => 'text',
                'placeholder' => $this->rp_prefix,
                'default'     => '',
                'disabled'    => true,
            ],
            'rp_development' => [
                'title'       => 'Development Mode',
                'description' => '',
                'type'        => 'checkbox',
                'default'     => 'yes',
            ],
            'rp_ftp_update' => [
                'title' => 'Update Now',
                'description' => 'Updates are pulled from Red Prairie hourly. The last update was <strong>'.($this->last_update? self::timeAgo($this->last_update) :'never').'</strong>.',
                'type' => 'button'
            ]
        ];
    }

    private static function timeAgo($timestamp) {
        $elapsed_time = time() - $timestamp;
        if ($elapsed_time < 1) { return '0 seconds'; }

        return self::timeFromSeconds($elapsed_time) . ' ago';
    }

    private static function timeFromSeconds($seconds, $largest_time = true, $singular_label = false, $use_short_times = false) {
        if ($seconds < 1) {
            return '0 seconds';
        }

        $time = array( 365 * 24 * 60 * 60  =>  'year',
                        30 * 24 * 60 * 60  =>  'month',
                             24 * 60 * 60  =>  'day',
                                  60 * 60  =>  'hour',
                                       60  =>  'minute',
                                        1  =>  'second'
                );

        $short_times = array( 'year'   => 'yr',
                              'month'  => 'mon',
                              'day'    => 'd',
                              'hour'   => 'hr',
                              'minute' => 'min',
                              'second' => 'sec'
                       );
        $return = array();

        foreach ($time as $num_seconds => $label) {
            $current_time = $seconds / $num_seconds;
            if ($current_time >= 1) {
                $round = round($current_time);

                $seconds = $seconds - ($num_seconds * $round);

                $return[] = $round . ' ' . ($use_short_times ? $short_times[$label] : $label).($round > 1 && !$singular_label ? 's' : '');
            }
        }

        return ($largest_time ? $return[0] : implode(', ', $return));
    }


    public function sendOrders() {
        $orders = new \WP_Query([
            'post_status' => ['wc-processing'],
            'post_type' => 'shop_order',
            'posts_per_page' => -1,
            'meta_query' => [
                [
                    'key' => '_rp_ordent',
                    'compare' => 'NOT EXISTS',
                ]
            ]
        ]);

        foreach($orders->posts as $order) {
            $this->orderPaid($order->ID);
        }
    }

    public function orderUpdated($order_id) {
        $this->orderPaid($order_id, 'U');
    }

    public function orderDelete($order_id) {
        $this->orderPaid($order_id, 'D');
    }


    public function orderSaved($order_id) {
        if(get_post_type($order_id) !== 'shop_order') { return true; }

        $this->orderPaid($order_id);
    }


    public function orderPaid($order_id = null, $mode = null) {
        $rows = [];

        if(!$order_id) { return false; }

        $rp_ordent = get_post_meta($order_id, '_rp_ordent', true);

        $order = new \WC_Order($order_id);
        $order_status = $order->get_status();

        if(!$rp_ordent && in_array($order_status, [
            'pending', 'failed', 'on-hold', 'cancelled', 'refunded', 'completed', 'draft', 'auto-draft'
        ])) {
            return false;
        }

        $order_date = strtotime($order->get_date_created());
        $order_items = $order->get_items();

        $shipping_method = @array_shift($order->get_shipping_methods());

        $delivery_date = '';
        $delivery_to_date = '';
        $delivery_method = '';

        $days    = null;
        $to_days = null;
        $weekend = null;
        $cutoff  = null;

        if($shipping_method['method_id'] && ($parts = explode(':', $shipping_method['method_id']))) {
            if($parts[0] === 'flat_rate' && isset($parts[1])) {
                if($method = new \WC_Shipping_Flat_Rate($parts[1])) {

                    $days    = (int) $method->get_option('shipping_days') ?: 3;
                    $to_days = (int) $method->get_option('shipping_to_days') ?: $days;
                    $weekend = $method->get_option('weekend_delivery', 'no') === 'yes' ? true : false;
                    $cutoff  = (int) $method->get_option('cutoff_time', 1200);

                    update_post_meta($order_id, '_rp_shipping', [
                        'shipping_days'    => $days,
                        'shipping_to_days' => $to_days,
                        'weekend_delivery' => $weekend,
                        'cutoff_time'      => $cutoff
                    ]);

                }
            }
        }

        if(!$delivery_date && get_post_meta($order_id, '_rp_shipping', true)) {
            $shipping_method = get_post_meta($order_id, '_rp_shipping', true);

            $days    = (int) $shipping_method['shipping_days'];
            $to_days = (int) $shipping_method['shipping_to_days'];
            $weekend = (bool) $shipping_method['weekend_delivery'];
            $cutoff  = (int) $shipping_method['cutoff_time'];
        }

        if($days && $to_days && $cutoff) {
            if((int) date('Gi') > $cutoff) {
                $days++;
                $to_days++;
            }

            $delivery_date    = strtotime("today +{$days} ".(!$weekend?'week':'')."days");
            $delivery_to_date = strtotime("today +{$to_days} ".(!$weekend?'week':'')."days");

            $delivery_date    = date('Ymd', $delivery_date);
            $delivery_to_date = $delivery_date !== $delivery_to_date ? date('Ymd', $delivery_to_date) : '';

            $delivery_method  = 'BY';
        }

        switch($order_status) {
            case 'failed':
            case 'on-hold':
            case 'cancelled':
            case 'refunded':
                $mode = 'D';
                update_post_meta($order_id, '_rp_ordent', false);
                break;
            default:
                $mode = 'C';
        }

        $batch_id = $order_id.time();

        $total_qty = 0;

        $dev = $this->rp_development === 'yes' ? 'T' : 'P';

        $rows[] = [
            'HDR', '4.7',$this->rp_client,$batch_id,'ORDENT',date('Ymd'),date('His'),date('Ymd'),date('His'),$dev,''
        ];

        foreach($order_items as $item) {
            $total_qty += $item->get_quantity();
        }

        $rows[] = [
            'BEG',                                                                  // Record Type
            $mode,                                                                  // Access Type
            $this->rp_ware,                                                         // Warehouse IE Ref.
            $this->rp_client,                                                       // Client Division IE Ref.
            date('Ymd'),                                                            // Client Create Date
            date('Gis'),                                                            // Client Create Time
            date('Ymd', $order_date),                                               // Order Date
            $this->rp_prefix.$order_id,                                             // Client Order Ref
            $this->rp_prefix.$order_id,                                             // Our Order Ref
            '',                                                                     // Source Name
            '',                                                                     // Source Address1
            '',                                                                     // Source Address2
            '',                                                                     // Source Town
            '',                                                                     // Source County
            '',                                                                     // Source Country
            '',                                                                     // Source Postcode
            '',                                                                     // Source Telephone
            '',                                                                     // Source Location Ref
            '',                                                                     // Source Location Desc
            '',                                                                     // Source Consignee Ref
            $order->get_shipping_first_name().' '.$order->get_shipping_last_name(), // Dest Name
            $order->get_shipping_address_1(),                                       // Dest Address1
            $order->get_shipping_address_2(),                                       // Dest Address2
            $order->get_shipping_city(),                                            // Dest Town
            $order->get_shipping_state(),                                           // Dest County
            $order->get_shipping_country(),                                         // Dest Country
            $order->get_shipping_postcode(),                                        // Dest Postcode
            $order->get_billing_phone(),                                            // Dest Telephone
            '',                                                                     // Dest Location Ref.
            '',                                                                     // Dest Location Desc
            '',                                                                     // Dest Consignee Ref
            '',                                                                     // Haulier Name,
            $delivery_date,                                                         // Delivery Date,
            $delivery_to_date,                                                      // Delivery To Date
            $delivery_method,                                                       // Delivery Method
            '',                                                                     // Delivery From Time
            '',                                                                     // Delivery To Time
            '',                                                                     // Contact,
            $order->get_customer_note(),                                            // Delivery Comments
            '',                                                                     // Internal Comments
            'Standard',                                                             // Order Type,
            'HU',                                                                   // C&E Order Type
            'Standard',                                                             // Delivery Type
            count($order_items),                                                    // Advised order lines
            $total_qty,                                                             // Advised Items
            '',                                                                     // Advised Containers,
            '',                                                                     // Advised Lifts
            '',                                                                     // Advised Linear Metres
            '',                                                                     // Advised Total Volume
            '',                                                                     // Advised Total Weight
            '',                                                                     // Prepack Qty
            count($order_items),                                                    // Expected Line Count
            '',                                                                     // Spare 1
            '',                                                                     // Spare 2
            '',                                                                     // Spare 3
            '',                                                                     // Spare 4
            '',                                                                     // Spare 5
            '',                                                                     // Store Number
            '',                                                                     // Spare 6
            '',                                                                     // Spare 7
            '',                                                                     // Spare 8
            '',                                                                     // Spare 9
            '',                                                                     // Spare 10
        ];

        foreach($order_items as $i => $order_item) {
            $product = $order_item->get_product();

            $rows[] = [
                'DET',                          // Record Type
                $mode,                          // Access Type
                $i,                             // Line Number
                $product->get_sku(),            // EDI Product Code
                $order_item->get_quantity(),    // Order Quantity,
                get_post_meta($product->get_id(), '_rp_uom', true) ?: 'EACH',                         // Unit of Measure
                '',                             // Containers
                '',                             // Lifts
                '',                             // Linear Metres
                '',                             // Volume
                '',                             // Weight
                '',                             // Driving Attribute From
                '',                             // Driving Attribute To
                '',                             // 2nd Attr From
                '',                             // 2nd Attr To
                '',                             // Notes
                '',                             // Notes2
                number_format($order_item->get_total(), 2, '.', ''), // Line Value
                '',                             // Customs Procure Code
                '',                             // Pack Type
                '',                             // Rotation Number From
                '',                             // Rotation Number To
                '',                             // POD Requred
                '',                             // Age Restriction
                '',                             // Spare 1
                '',                             // Spare 2
                '',                             // Spare 3
                '',                             // Spare 4
                '',                             // Spare 5
                '',                             // Country of Manufacture
                '',                             // Fabric Content
                '',                             // Harmonised Product Code
                '',                             // Type
                '',                             // Gender
                '',                             // Unit Weight
                '',                             // Spare 6
                '',                             // Spare 7
                '',                             // Spare 8
                '',                             // Spare 9
                '',                             // Spare 10
            ];
        }

        $rows[] = [
            'TRL',
            $batch_id,
            count($rows) + 1,
            ''
        ];

        if($this->rp_development !== 'yes' && $mode !== 'D') {
            update_post_meta($order_id, '_rp_ordent', true);
        }

        return $this->sendToFTP('ORDENT-'.$order_id.'-'.time(), $rows, $order);
    }


    private function createCSV($filename, $rows) {
        $uploads = wp_upload_dir()['basedir'];
        $path = $uploads.DIRECTORY_SEPARATOR.'wc-redprairie';
        $file = $path.DIRECTORY_SEPARATOR.$filename.'.csv';

        if(!file_exists($path) || !is_dir($path)) {
            mkdir($path);
        }

        $output = fopen($file,'w+') or die("Can't open {$file}");

        foreach($rows as $row) {
            fputcsv($output, $row);
            fseek($output, -1, SEEK_CUR);
            fwrite($output, "\r\n");
        }

        fclose($output) or die("Can't close {$file}");

        return $file;
    }

    private function ftpConnect($passive = false) {
        if($this->ftp) return;

        $this->ftp = ftp_connect(RP_HOST);
        ftp_login($this->ftp, RP_USER, RP_PASS);

        if($passive) ftp_pasv($this->ftp, true);

        return $this->ftp;
    }

    private function ftpClose() {
        if(!$this->ftp) return;

        return ftp_close($this->ftp);
    }

    public function ftpCd($path) {
        $this->ftpConnect();

        $path = trim($path, " \t\n\r\0\x0B\\\/");
        $path_parts = array_filter(explode('/', $path));
        $cur_path = '';

        if($path_parts) {
            foreach($path_parts as $part) {
                $cur_path .= "/{$part}";

                if(!ftp_nlist($this->ftp, $cur_path)) {
                    ftp_mkdir($this->ftp, $part);
                }

                ftp_chdir($this->ftp, $cur_path);
            }
        }

        return $path;
    }


    private function sendToFTP($filename, $rows, $order = null) {
        $file = $this->createCSV($filename, $rows);

        $this->ftpConnect();

        $path = defined('RP_PATH') ? RP_PATH : null;
        $path = $this->ftpCd("/{$path}/Inbound/");

        $filename = pathinfo($file, PATHINFO_BASENAME);
        $put = ftp_put($this->ftp, "/{$path}/{$filename}", $file, FTP_BINARY);

        if($order) {
            if($put) {
                $order->add_order_note("The file <code>{$filename}</code> was sent successfully to <code>{$path}</code>.");
            } else {
                $order->add_order_note("An error occurred when sending the file <code>{$filename}</code>.");
            }
        }

        $this->ftpClose();
    }

    private function parseRaw($data, $directories = false) {
        $files = array();
        $filetypes = array('-'=>'file', 'd'=>'directory');

        $systyp = ftp_systype($this->ftp);

        foreach($data as $line) {

            $item = [];

            if(substr($line,0,5) == "total") {

                continue;

            } elseif($systyp=="Windows_NT") {

                if(preg_match('/([-0-9]+ *[0-9:]+[PA]?M?) +<DIR> {10}(.*)/',$line, $matches)) {
                    if(!$directories) { continue; }

                    $date = date_parse_from_format('m-d-y '.($matches[1]{-1} === 'M'?'h:iA':'H:i'),$matches[1]);

                    $files[$matches[2]] = [
                        'type' => 'directory',
                        'size' => 0,
                        'date' => (int) mktime(
                            $date['hour'],
                            $date['minute'],
                            $date['second'],
                            $date['month'],
                            $date['day'],
                            $date['year']
                        ),
                    ];

                } elseif(preg_match('/([-0-9]+ *[0-9:]+[PA]?M?) +([0-9]+) (.*)/',$line, $matches)) {
                    if($directories) { continue; }

                    $date = date_parse_from_format('m-d-y '.($matches[1]{-1} === 'M'?'h:iA':'H:i'),$matches[1]);

                    $files[$matches[3]] = [
                        'type' => 'file',
                        'size' => $matches[2],
                        'date' => (int) mktime(
                            $date['hour'],
                            $date['minute'],
                            $date['second'],
                            $date['month'],
                            $date['day'],
                            $date['year']
                        ),
                    ];
                }


            } elseif($systyp=="UNIX") {
                preg_match('/'. str_repeat('([^\s]+)\s+', 7) .'([^\s]+) (.*)/', $line, $matches);
                $item = [];

                list($item['rights'], $item['number'], $item['user'], $item['group'], $item['size'], $month, $day, $time, $name) = array_slice($matches, 1);

                $type = array_key_exists($item['rights']{0}, $filetypes) ? $filetypes[$item['rights']{0}] : false;

                if(!$type || ($directories && $type !== 'directory') || (!$directories && $type === 'directory')) { return; }

                $item['date'] = (int) strtotime("{$month} {$day} {$time}");

                $files[$name] = $item;

            }
        }

        return $files;
    }


    public function importFiles() {
        $this->ftpConnect();

        $path = defined('RP_PATH') ? RP_PATH : null;
        $path = $this->ftpCd("/{$path}/Outbound/");

        $files = ftp_rawlist($this->ftp, "/{$path}");

        foreach($this->parseRaw($files) as $name => $file) {

            $csv = array_map('str_getcsv', file("ftp://".RP_USER.":".RP_PASS."@".RP_HOST."/{$path}/{$name}", FILE_SKIP_EMPTY_LINES));

            if(!$csv) continue;

            $date = $csv[0][5].' '.$csv[0][6];

            $date = date_parse_from_format('Ymd His', $date);

            if($csv[0][2] !== RP_CLIENT) continue;

            $timestamp = mktime(
                $date['hour'],
                $date['minute'],
                $date['second'],
                $date['month'],
                $date['day'],
                $date['year']
            );

            switch($csv[0][4]) {
                case 'PRODBAL':
                    $this->updateBalances($csv, $timestamp);
                    $this->ftpMove("/{$path}/{$name}", "/{$path}/archive/{$name}");
                    break;
                case 'CONFDESP':
                case 'DESPCONF':
                    $this->despatchConfirmation($csv, $timestamp);
                    $this->ftpMove("/{$path}/{$name}", "/{$path}/archive/{$name}");
                    break;
                default:
                    continue;
                    break;
            }
        }

        update_option('woocommerce_rp_last_update', time(), true);
    }

    private function stripHeaders($csv) {
        array_pop($csv);
        array_shift($csv);

        return $csv;
    }

    private function updateBalances($csv, $timestamp) {
        $csv = $this->stripHeaders($csv);

        foreach($csv as $row) {
            $sku   = $row[5];
            $stock = (int) $row[12];
            $uom   = $row[10];

            $product_id = wc_get_product_id_by_sku($sku);
            $product = wc_get_product($product_id);

            $updated = get_post_meta($product_id, '_rm_last_stock_update', true) ?: 0;

            if($updated < $timestamp) {

                wc_update_product_stock($product_id, $stock);

                update_post_meta($product_id, '_rp_uom', $uom);
                update_post_meta($product_id, '_rm_last_stock_update', $timestamp);

            }
        }

        return true;
    }

    private function despatchConfirmation($csv, $timestamp) {
        $csv = $this->stripHeaders($csv);

        $order_id = 0;
        $order = null;

        foreach($csv as $row) {
            if($row[0] === 'BEG') {
                $order_id = $row[20];
                $order_id = preg_replace("/^({$this->rp_prefix})/", '', $order_id);

                if($order = wc_get_order($order_id)) {

                    $updated = get_post_meta($order_id, '_rm_last_update', true) ?: 0;

                    if($updated < $timestamp) {

                        $order->update_status('completed', 'Received dispatch confirmation from Red Prairie.<br><br>');
                        update_post_meta($order_id, '_rm_last_update', $timestamp);

                    }
                }
            }
        }

        return true;
    }

    private function ftpCopy($remote_file, $new_file) {
        $this->ftpConnect();

        $name = pathinfo($remote_file,PATHINFO_BASENAME);
        $path = pathinfo($new_file, PATHINFO_DIRNAME);

        if(ftp_get($this->ftp, sys_get_temp_dir().'/'.$name, $remote_file, FTP_BINARY)) {
            $this->ftpCd($path);
            if(ftp_put($this->ftp, $new_file, sys_get_temp_dir().'/'.$name, FTP_BINARY)) {
                unlink(sys_get_temp_dir().'/'.$name);
                return true;
            }
        }

        return false;
    }

    private function ftpMove($remote_file, $new_file) {
        $this->ftpConnect();

        if($this->ftpCopy($remote_file, $new_file)) {
            ftp_delete($this->ftp, $remote_file);
            return true;
        }

        return false;
    }
}
